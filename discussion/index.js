let posts = [];
let count = 1;

// add post data

document.querySelector('#form-add-post').addEventListener('submit', (e)=>{
	// prevents the page from loading
	e.preventDefault();
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})

	count++;

	showPosts(posts)
	alert('Movie post successfully added')
})

// show posts
const showPosts = (posts)=> {
	let postEntries = ' '

	posts.forEach((post) =>{
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// edit post
/*
const editPost = {
	document.querySelector('#form-edit-post').addEventListener('submit', (e)=>{
		e.preventDefault();
		posts.splice({
			id: document.querySelector('#txt-edit-id').value,
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value
		})

		showPosts(posts)
		alert('Movie post successfully updated')
	})
}
*/



const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}


// update post
document.querySelector(`#form-edit-post`).addEventListener('submit', (e)=>{
	e.preventDefault()

	// id = 1 index 0
	// posts = 2 elements, 0, 1
	// posts.length = 2

	for (let i = 0; i < posts.length; i++) {
		// String 1 == Number 1
		// String 1 === Number 1

		if (posts[i].id.toString()===document.querySelector(`#txt-edit-id`).value) {
				posts[i].title = document.querySelector(`#txt-edit-title`).value
				posts[i].body = document.querySelector(`#txt-edit-body`).value

				showPosts(posts)
				alert('Movie post successfully updated')
				break
		}
	}

})


const deletePost = (id) => {
	let entry = document.querySelector(`#post-entry-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}
